Download antergos iso: https://antergos.com/download/antergos-live-iso/

Create bootable usb: etcher https://etcher.io/, or your choice

Go through Cinchi to the format hard drive part
Click on the main drive for linux, create new partition table
Select MBR
Create boot partition: 550MB fat32 mount /boot
Create root partition: rest f2fs mount /
Finish the Cinchi install


After installation:
Step through the initial.sh script to get all desired packages

Configs:
Specific for Nvidia/Nouveau
In /etc/mkinitcpio.conf add nouveau for early kms, other modules are separated by spaces
---
MODULES="nouveau"

Specific for lm_sensors / gnome-shell-extension-freon:
add /etc/sensors.d/Nvidia
    chip "nouveau-pci-0100"
        label temp1 "GPU temp"
        label fan1 "GPU fan"

add /etc/sensors.d/Intel
    chip "coretemp-isa-0000"
        label temp1 "CPU core"