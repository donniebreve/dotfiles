#!/bin/bash
# aur packages I want to install, requires aur-install.sh

# vdpau not working for NVidia GTX 960 yet, see initial.sh
#./aur-install.sh nouveau-fw

./aur-install.sh numix-circle-icon-theme-git
./aur-install.sh numix-icon-theme-git
./aur-install.sh gnome-shell-extension-weather-git
./aur-install.sh gnome-shell-extension-freon
./aur-install.sh gnome-shell-extension-activities-config
./aur-install.sh gnome-shell-extension-taskbar
./aur-install.sh insomnia
./aur-install.sh slack-desktop
./aur-install.sh spotify-stable
./aur-install.sh tor-browser
