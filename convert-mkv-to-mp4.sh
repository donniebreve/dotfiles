#!/bin/bash
# aur installation helper script

echo "convert mkv to mp4 and subtitles to srt"

if [ -z "$1" ]
  then
    echo "no argument supplied"
    exit 1
fi

echo $PWD

#for i in *.mkv
#do
    if test -f "$1"
    then
        file=$(basename "$1")
        filename=$(echo "$file" | sed -r 's/\.mkv$//g')
        ffmpeg -i "$file" -map 0:v:0 -map 0:a:0 -sn -c copy "$filename.mp4"
        ffmpeg -i "$file" -map 0:s:0 "$filename.srt"
    fi
#done