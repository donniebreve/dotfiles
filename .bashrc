#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls -laF --color=auto'

# Pacman shortcuts
#alias pacman-install='sudo pacman -S'
#alias pacman-search-local='pacman -Qs'
#alias pacman-search-online='pacman -Ss'
#alias pacman-remove='sudo pacman -Rns'
#alias pacman-update='sudo pacman -Syu'
#alias pacman-mirror-update='reflector --country US --protocol https -i .edu --sort rate | sudo tee /etc/pacman.d/mirrorlist'
pacman() {
        if [[ $1 == "install" ]]; then
                command sudo pacman -S $2
        elif [[ $1 == "installed" ]]; then
                command pacman -Qs $2
        elif [[ $1 == "uninstall" ]]; then
                command sudo pacman -Rns $2
        elif [[ $1 == "update" ]]; then
                command sudo pacman -Syu
        elif [[ $1 == "mirrors" ]]; then
                command sudo reflector --country US --protocol https -i .edu --sort rate | sudo tee /etc/pacman.d/mirrorlist
        fi
}

# Shows user, machine, and dir, colored
Default='\033[0m'
Green='\033[32m';
Red='\033[31m';
Blue='\033[34m';
Yellow='\033[33m';
PS1="[${Blue}\h ${Blue}\t${Default}][${Yellow}\w${Default}]: "
