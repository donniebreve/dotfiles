#!/bin/bash
# Initial installation script

# base
sudo pacman -S git ntfs-3g termite ufw ttf-dejavu tamsyn-font

# start ufw
sudo systemctl enable ufw.service
sudo ufw enable

# sway
#sudo pacman -S sway i3status

# gnome
sudo pacman -S gdm gnome-control-center nautilus networkmanager gedit

# start networkmanager
sudo systemctl enable NetworkManager.service
sudo systemctl start NetworkManager.service

# gnome theme
sudo pacman -S gnome-tweaks adapta-gtk-theme

# additional gnome applications
sudo pacman -S evince baobab file-roller gnome-calculator gnome-calendar gnome-clocks gnome-color-manager gnome-disk-utility gnome-menus gnome-shell-extensions gnome-system-monitor gvfs gvfs-afc gvfs-mtp gvfs-nfs gvfs-smb

# utility applications
sudo pacman -S keepass virtualbox virtualbox-host-modules-arch virtualbox-guest-iso clamav rkhunter

# media applications
sudo pacman -S pulseaudio pavucontrol vlc handbrakecli

# video acceleration, does not currently work for NVidia GTX 960, requires nouveau-fw from AUR when/if it works
#sudo pacman -S mesa-vdpau vdpauinfo libva-vdpau-driver libva-utils

# work
sudo pacman -S libreoffice-fresh thunderbird

# projects
sudo pacman -S atom mongodb mongodb-compass mongodb-tools