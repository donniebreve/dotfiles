for i in *
do
    if test -f "$i"
    then
        new=$(echo "$i" | sed -r 's/\[[^]]+\]|\([^\)]+\)//g')
        new=$(echo "$new" | sed -r 's/^[ ]+//g')
        new=$(echo "$new" | sed -r 's/[ ]+\./\./g')
        echo "old:$i new:$new"
        read -p "rename? (y/n)" rename
        if [ $rename = "y" ]
        then
            mv "$i" "$new"
        fi
    fi
done