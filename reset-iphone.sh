#!/bin/bash
# aur installation helper script

echo ":: reset iphone usb port ::"
echo "when plugging in the phone more than once, it is required to reset the usb port to have the iphone charge again"
echo ""

# get results
result=( $(dmesg | grep "Product: iPhone" | sed -e "s/\[[^]]\+\] usb //" -e "s/:.\+//" | tail -n 1) )

echo "Found iPhone on usb bus-device: $result"

if [ -d "/sys/bus/usb/devices/$result" ]; then
    sudo sh -c "echo 0 > /sys/bus/usb/devices/$result/authorized"
    sudo sh -c "echo 1 > /sys/bus/usb/devices/$result/authorized"
    echo "iPhone usb reset"
else
    echo "iPhone device not found"
fi